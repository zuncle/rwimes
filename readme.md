# Rwimes library (interface mode)

## Install library in Rstudio

    > install.packages("devtools")
    > library("devtools")
    > install_bitbucket(repo="zuncle/rwimes")
    
## Test
    
Check the IP Address of the Wimes host computer and set the variable "url" to the right value:

    > url <- "WIMES ip address or host name" (e.g url <- "168.192.1.10")

Check the port of the Wimes host computer and set the variable "port" to the right value:
    
    > port <- "9000"

Execute the following function to get the list of ponctual sites:    
    
    > getPonctualSites(url, port)
    
## Create a package from R files
https://support.rstudio.com/hc/en-us/articles/200486488-Developing-Packages-with-RStudio

1. In RStudio, use the "File" menu then "New project" options
1. From the New Project dialog box, select "New Directory", then "R Package"
1. Enter Package name, add existing R files then click on "Create Project"
1. In the "Build" Pane, Click on the button "Build & Reload" to compile and build the package library
1. The package is now installed locally (exact path is given in the Build log)
1. Have a look to the "Package" pane to check the package installed
1. You can now push your new package in a git repository, such as bitbucket, see below

## Publish a R package on bitbucket

1. Create an account on bitbucket (http://bitbucket.org) if needed (e.g. "myname")
1. Create a new bitbucket repository for the package (e.g. "myPackage")
1. Follow the instructions given in "Overview" navigation pane to publish your R package from your local repository to the bitbucket repository

When completed, your library is now avaible by using the following commands in RStudio:

     > install.packages("devtools")
     > library("devtools")
     > install_bitbucket(repo="myname/mypackage")

## Tips and tricks for developers

### How to commit and push to zuncle/rwimes

     > git commit -m 'title of the commit'
     > git push -u origin master
     
### Remove library rwimes from RStudio

     > remove.packages("rimes")

### Clear workspace

     > rm(list=ls())